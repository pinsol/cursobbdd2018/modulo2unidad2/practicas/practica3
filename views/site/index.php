<?php
    use yii\helpers\Html;
    
/* @var $this yii\web\View */

$this->title = 'Práctica 3';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas</h1>

        <p class="lead">Bloque inicial de consultas</p>
        <br>
        <center>
            <div class="col-mod-2 pad-1">
            <?= Html::a('Todos los trabajadores', ['trabajadores/consulta12'], ['class'=> 'btn btn-default']) ?>
            </div>
        </center>
        <br>
        <center>
            <div class="col-mod-2 pad-1">
            <?= Html::a('Todos las delegaciones', ['delegacion/consulta14'], ['class'=> 'btn btn-default']) ?>
            </div>
        </center>
        <br>
        <center>
            <div class="col-mod-2 pad-1">
            <?= Html::a('Delegaciones de Santander', ['site/consulta17a'], ['class'=> 'btn btn-default']) ?>
            </div>
        </center>
        <br>
        <center>
            <div class="col-mod-2 pad-1">
            <?= Html::a('Trabajadores de la Delegación 1', ['site/consulta17b'], ['class'=> 'btn btn-default']) ?>
            </div>
        </center>
        <br>
        <center>
            <div class="col-mod-2 pad-1">
            <?= Html::a('Trabajadores ordenados por nombre', ['site/consulta17c'], ['class'=> 'btn btn-default']) ?>
            </div>
        </center>
        <br>
        <center>
            <div class="col-mod-2 pad-1">
            <?= Html::a('Trabajadores sin fecha de nacimiento', ['site/consulta17d'], ['class'=> 'btn btn-default']) ?>
            </div>
        </center>
    </div>  

</div>
