<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index-index">

    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns'=>
            $columnas,
        
    ]); ?>
    
</div>
