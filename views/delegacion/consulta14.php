<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consulta 2: Todas las delegaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <h2>Utilizando ACTIVERECORD (ActiveQuery)</h2>
    <div>
        <?php
            var_dump($resultado);
        ?>
    </div>
    
    <h2>Utilizando COMMAND</h2>
        <div>
        <?php
            var_dump($consulta);
        ?>
    </div>
    
    <h2>Utilizando QUERYBUILDER</h2>
    <div>
        <?php
            var_dump($listado);
        ?>
    </div>
</div>
