<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Delegacion;
use app\models\Trabajadores;
use yii\helpers\ArrayHelper;




class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    // LISTAR LAS DELEGACIONES CUYA POBLACION SEA SANTANDER
    public function actionConsulta17a(){
       /**
        * Utilizando ActiveRecord
        */ 
       $r= Delegacion::find()
               ->where("poblacion like 'santander'")
               ;
              
       
       $resultado=$r->all();
              
        
        /**
         * Crear una consulta con Command
         */
        
        $consulta=Yii::$app->db
                ->createCommand("select * from delegacion where poblacion like 'santander'")
                ->queryAll();
        
       
       return $this->render("consulta17",[
           "datos"=>$resultado,
           "datos1"=>$consulta,
       ]);
    }
    // LISTAR LOS TRABAJADORES DE LA DELEGACION 1
    public function actionConsulta17b(){
       /**
        * Utilizando ActiveRecord
        */ 
       $r= Trabajadores::find()
               ->where("id=1")
               ;
              
       
       $resultado=$r->all();
       
        
        /**
         * Crear una consulta con Command
         */
        
        $consulta=Yii::$app->db
                ->createCommand("select * from delegacion where id=1")
                ->queryAll();
        
       
       return $this->render("consulta17",[
           "datos"=>$resultado,
           "datos1"=>$consulta,
       ]);
    }
    
    // LISTAR LOS TRABAJADORES ORDENADOS POR NOMBRE
    public function actionConsulta17c(){
       /**
        * Utilizando ActiveRecord
        */ 
       $r= Trabajadores::find()
               ->orderBy("nombre")
               ;
              
       
       $resultado=$r->all();
       
        
        /**
         * Crear una consulta con Command
         */
        
        $consulta=Yii::$app->db
                ->createCommand("select * from trabajadores order by 'nombre'")
                ->queryAll();
        
       
       return $this->render("consulta17",[
           "datos"=>$resultado,
           "datos1"=>$consulta,
       ]);
    }
    
    // LISTAR LOS TRABAJADORES QUE NO CONOZCO LA FECHA DE NACIMIENTO
    public function actionConsulta17d(){
       /**
        * Utilizando ActiveRecord
        */ 
       $r= Trabajadores::find()
               ->where("fechaNacimiento is null")
               ;
              
       
       $resultado=$r->all();
       
        
        /**
         * Crear una consulta con Command
         */
        
        $consulta=Yii::$app->db
                ->createCommand("select * from trabajadores where fechaNacimiento is null")
                ->queryAll();
        
       
       return $this->render("consulta17",[
           "datos"=>$resultado,
           "datos1"=>$consulta,
       ]);
    }
    // LISTAR LAS DELEGACIONES QUE TENGAN UNA POBLACION DISTINTA A SANTANDER Y DE LAS CUALES SEPAMOS LA DIRECCION
    public function actionConsulta20a(){
       
        $consulta= Delegacion::find()
                ->where("poblacion not like 'Santander' and
                        direccion is not null");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta20",[
            "datos"=>$dp,
             "columnas"=>[
                'id',
                'nombre',
                'poblacion',
                 'direccion',
            ]
        ]);
    }
    // LISTAR TODOS LOS TRABAJADORES DE LAS DELEGACIONES DE SANTANDER
    public function actionConsulta20b(){
        
        /*$santander= Delegacion::find()
                ->select("id")
                ->where(['poblacion'=>'santander'])
                ->asArray()
                ->all();
        
         $santander=ArrayHelper::map($santander, 'id', 'id');
        
               
        $consulta= Trabajadores::find()
                ->where(['in','delegacion',$santander]);*/
        
        
        $consulta= Trabajadores::find()
                ->joinWith('delegacion0')
                ->where(['poblacion'=>'santander']);
                    
                
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta20",[
            "datos"=>$dp,
             "columnas"=>[
                'id',
                'nombre',
                'delegacion0.nombre',
            ]
        ]);
    }
    // LISTAR LOS TRABAJADORES Y LAS DELEGACIONES DE LOS TRABAJADORES QUE TENGAN FOTO
    public function actionConsulta20c(){
       
        $consulta= Trabajadores::find()
                ->where("Trabajadores.foto is not null");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta20",[
            "datos"=>$dp,
            "columnas"=>[
                'id',
                'nombre',
                'delegacion0.nombre',
            ]
        ]);
    }
    // SACAR LAS DELEGACIONES QUE NO TIENEN TRABAJADORES
     public function actionConsulta20d(){
       
        $consulta= Trabajadores::find()
                ->joinWith("delegacion0")
                ->where("delegacion is null");
        
                
        $dp=new ActiveDataProvider([
          "query"=>$consulta
       ]);
        
        return $this->render("consulta20",[
            "datos"=>$dp,
            "columnas"=>[
                'id',
                'nombre',
                'delegacion0.nombre',
            ]
        ]);
    }
    
}
