<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delegacion".
 *
 * @property int $id
 * @property string $nombre
 * @property string $poblacion
 * @property string $direccion
 *
 * @property Trabajadores[] $trabajadores
 */
class Delegacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delegacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'poblacion'], 'string', 'max' => 20],
            [['direccion'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'poblacion' => 'Poblacion',
            'direccion' => 'Direccion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajadores()
    {
        return $this->hasMany(Trabajadores::className(), ['delegacion' => 'id']);
    }
}
